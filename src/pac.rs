//! Extensions for running PAC scripts.
//!
//! All extensions must implement the [`PacRunnerBuilder`] trait defined here. The implementation must
//! return an implementation of the [`PacRunner`] trait which is responsible for actually executing a single
//! PAC script (possibly multiple times).
//!
//! [`PacRunnerBuilder`]: trait.PacRunnerBuilder.html
//! [`PacRunner`]: trait.PacRunner.html

use std::io::Read;

use url::Url;

use crate::error::Result;

#[cfg(feature = "spidermonkey")]
pub mod mozjs;
pub mod util;

/// Trait for PAC runners.
///
/// PAC runners are usually called multiple times with different URLs and execute
/// a PAC script to get the proxies for the given URL.
pub trait PacRunner {
    /// Get the proxies for an URL.
    fn get_proxy_for_url(&mut self, url: &Url) -> Result<String>;
}

/// Trait for PAC runner builders. This trait must be implemented by extensions.
///
/// Implementors are responsible for setting up a [`PacRunner`] for a given PAC script.
///
/// [`PacRunner`]: trait.PacRunner.html
pub trait PacRunnerBuilder {
    /// Build a PAC runner from a given PAC script.
    fn build_pac_runner(&self, pac_script: &mut dyn Read) -> Result<Box<dyn PacRunner>>;
    /// Get log tag for this extension.
    fn log_tag(&self) -> &'static str;
}
