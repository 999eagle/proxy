//! Utility functions for the crate.

use bytes::{Buf, Reader};
use http::{Request, Response};
use hyper::{
    rt::{Future, Stream},
    Body, Chunk, Client,
};
use url::Url;

use crate::error::{ProxyConfigError, Result};

/// Parse a URL and prepend a default scheme if the given URL doesn't have a scheme.
pub fn parse_url_with_default_scheme(url: &str, default_scheme: &str) -> Result<Url> {
    let split: Vec<&str> = url.split("://").collect();
    match split.len() {
        2 => Ok(Url::parse(url)?),
        1 => Ok(Url::parse(&format!("{}://{}", default_scheme, url))?),
        _ => Err(ProxyConfigError::InvalidConfig),
    }
}

/// Get the URL for a no proxy.
pub fn direct_url() -> Url {
    Url::parse("direct://").expect("this doesn't fail")
}

/// Get the URL for determining the proxy using WPAD.
#[allow(dead_code)] // function isn't used in all configurations
pub fn wpad_url() -> Url {
    Url::parse("wpad://").expect("this doesn't fail")
}

/// Make a synchronous HTTP request to download a PAC script.
pub fn get_pac_script_from_uri(uri: &str) -> Result<Response<Body>> {
    let request = Request::builder()
        .uri(uri)
        .header("Accept", "application/x-ns-proxy-autoconfig")
        .body(Body::empty())
        .map_err(ProxyConfigError::from_error)?;

    let client = Client::new();
    let response = client
        .request(request)
        .wait()
        .map_err(ProxyConfigError::from_error)?;
    Ok(response)
}

/// Convert a `Body` into a `Reader` implementing `std::io::Read`.
pub fn body_into_read(body: Body) -> Result<Reader<Chunk>> {
    let read = body
        .concat2()
        .wait()
        .map_err(ProxyConfigError::from_error)?;
    Ok(read.reader())
}
