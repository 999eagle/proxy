//! Extension for the DNS Alias WPAD method.

use std::io::Read;

use log::trace;

use crate::util;

use super::{ProxyConfigError, Result, WPAD};

/// Log tag for this extension.
const LOG_TAG: &str = module_path!();

/// [`WPAD`] implementation for the DNS Alias WPAD method.
///
/// [`WPAD`]: ../trait.WPAD.html
#[allow(missing_copy_implementations)]
pub struct DNSAlias;

impl DNSAlias {
    /// Create new instance.
    pub const fn new() -> Self {
        Self {}
    }
}

impl Default for DNSAlias {
    fn default() -> Self {
        Self::new()
    }
}

impl WPAD for DNSAlias {
    fn get_wpad(&self) -> Result<Box<dyn Read>> {
        let hostname = dns_lookup::get_hostname().map_err(ProxyConfigError::from_error)?;
        let mut search_domains: Vec<String> = Vec::new();
        #[cfg(windows)]
        {
            if let Ok(domains) = ipconfig::computer::get_search_list() {
                search_domains = domains;
            }
        }
        if search_domains.is_empty() {
            search_domains.push("".into());
        }
        trace!(
            "starting with hostname {} in domains {:?}",
            hostname,
            search_domains
        );
        let _search_domains = search_domains
            .iter()
            .map(|domain| {
                let mut split = domain.split('.').map(ToOwned::to_owned).collect::<Vec<_>>();
                let len = split.len();
                if len >= 2 {
                    // we've checked the length
                    #[allow(clippy::indexing_slicing)]
                    {
                        split[len - 2] = format!("{}.{}", split[len - 2], split[len - 1]);
                    }
                }
                split
            })
            .collect::<Vec<_>>();
        let host_components = hostname.split('.').collect::<Vec<_>>();

        // Now that we have host name and search domains, for each search domain prepend the host
        // name and try to resolve the wpad well known alias
        for i in 0..host_components.len() {
            let mut url = host_components.iter().skip(i + 1).fold(
                "http://wpad".into(),
                |mut acc: String, val| {
                    acc.push('.');
                    acc.push_str(val);
                    acc
                },
            );
            url.push_str("/wpad.dat");
            let result = util::get_pac_script_from_uri(&url);
            if let Ok(response) = result {
                if response.status().is_success() {
                    return Ok(Box::new(util::body_into_read(response.into_body())?));
                }
            }
        }

        Err(ProxyConfigError::FailedToReadConfiguration)
    }

    fn log_tag(&self) -> &'static str {
        LOG_TAG
    }
}

#[cfg(test)]
mod tests {
    use crate::{error::ProxyConfigError, wpad::WPAD};

    use super::DNSAlias;

    #[test]
    fn test() {
        let wpad = DNSAlias::new();
        match wpad.get_wpad() {
            Ok(_) | Err(ProxyConfigError::FailedToReadConfiguration) => {}
            _ => panic!("failed to resolve wpad"),
        }
    }
}
