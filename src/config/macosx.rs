//! Extension for getting the proxy configuration from macOS system configuration.
#![allow(unsafe_code)]

use std::{error::Error, fmt};

use system_configuration::{
    core_foundation::{
        array::CFArray,
        base::{CFType, FromVoid, TCFType},
        dictionary::CFDictionary,
        number::CFNumber,
        string::CFString,
        ConcreteCFType,
    },
    dynamic_store::{SCDynamicStore, SCDynamicStoreBuilder},
    sys::{core_foundation_sys::string::CFStringRef, schema_definitions},
};
use url::Url;

use crate::util;

use super::{ProxyConfig, Result};

/// Log tag for this extension.
const LOG_TAG: &str = module_path!();

/// [`ProxyConfig`] implementation for getting the proxy configuration from macOS system configuration.
///
/// [`ProxyConfig`]: ../trait.ProxyConfig.html
pub struct MacOSXConfig {
    /// Store containing system configuration.
    store: SCDynamicStore,
}

impl MacOSXConfig {
    /// Create new instance.
    pub fn new() -> Self {
        let store = SCDynamicStoreBuilder::new("proxy-rs").build();
        Self { store }
    }
}

impl Default for MacOSXConfig {
    fn default() -> Self {
        Self::new()
    }
}

impl ProxyConfig for MacOSXConfig {
    fn get_config(&self, url: &Url) -> Result<Vec<Url>> {
        let proxies = match self.store.get_proxies() {
            None => return Ok(vec![util::direct_url()]),
            Some(proxies) => proxies,
        };

        if get_obj::<bool>(&proxies, unsafe {
            schema_definitions::kSCPropNetProxiesProxyAutoDiscoveryEnable
        })
        .unwrap_or(false)
        {
            return Ok(vec![util::wpad_url()]);
        }

        if get_obj::<bool>(&proxies, unsafe {
            schema_definitions::kSCPropNetProxiesProxyAutoConfigEnable
        })
        .unwrap_or(false)
        {
            if let Ok(url) = get_obj::<String>(&proxies, unsafe {
                schema_definitions::kSCPropNetProxiesProxyAutoConfigURLString
            }) {
                return Ok(vec![Url::parse(&format!("pac+{}", url))?]);
            }
        }

        if let Some(proxy) = scheme_proxy(&proxies, url.scheme()) {
            return Ok(vec![Url::parse(&proxy)?]);
        }

        Ok(vec![util::direct_url()])
    }

    fn get_ignore(&self, _url: &Url) -> Result<String> {
        let proxies = match self.store.get_proxies() {
            None => return Ok("".into()),
            Some(proxies) => proxies,
        };

        let exceptions = get_obj::<Vec<String>>(&proxies, unsafe {
            schema_definitions::kSCPropNetProxiesExceptionsList
        });
        let exceptions = match exceptions {
            Err(_err) => return Ok("".into()),
            Ok(exceptions) => exceptions.join(","),
        };
        Ok(exceptions)
    }

    fn log_tag(&self) -> &'static str {
        LOG_TAG
    }
}

/// Get the proxy for a specific URL scheme from the system configuration.
fn scheme_proxy(proxies: &CFDictionary<CFString, CFType>, scheme: &str) -> Option<String> {
    let (enable_key, proxy_key, port_key) = unsafe {
        match scheme {
            "ftp" => (
                schema_definitions::kSCPropNetProxiesFTPEnable,
                schema_definitions::kSCPropNetProxiesFTPProxy,
                schema_definitions::kSCPropNetProxiesFTPPort,
            ),
            "https" => (
                schema_definitions::kSCPropNetProxiesHTTPSEnable,
                schema_definitions::kSCPropNetProxiesHTTPSProxy,
                schema_definitions::kSCPropNetProxiesHTTPSPort,
            ),
            "gopher" => (
                schema_definitions::kSCPropNetProxiesGopherEnable,
                schema_definitions::kSCPropNetProxiesGopherProxy,
                schema_definitions::kSCPropNetProxiesGopherPort,
            ),
            "rtsp" => (
                schema_definitions::kSCPropNetProxiesRTSPEnable,
                schema_definitions::kSCPropNetProxiesRTSPProxy,
                schema_definitions::kSCPropNetProxiesRTSPPort,
            ),
            "socks" => (
                schema_definitions::kSCPropNetProxiesSOCKSEnable,
                schema_definitions::kSCPropNetProxiesSOCKSProxy,
                schema_definitions::kSCPropNetProxiesSOCKSPort,
            ),
            "http" | _ => (
                schema_definitions::kSCPropNetProxiesHTTPEnable,
                schema_definitions::kSCPropNetProxiesHTTPProxy,
                schema_definitions::kSCPropNetProxiesHTTPPort,
            ),
        }
    };

    let enable = get_obj::<bool>(proxies, enable_key).unwrap_or(false);
    if !enable {
        return None;
    }

    let port = get_obj::<i64>(proxies, port_key);
    let proxy = get_obj::<String>(proxies, proxy_key);
    let (port, proxy) = match (port, proxy) {
        (Ok(port), Ok(proxy)) => (port, proxy),
        _ => return None,
    };

    let proxy_scheme = match scheme {
        "rtsp" => "rtsp",
        "socks" => "socks",
        "gopher" | "ftp" | "https" | "http" | _ => "http",
    };
    Some(format!("{}://{}:{}", proxy_scheme, proxy, port))
}

/// Get a key from a `CFDictionary` and convert the value to a rust value.
fn get_obj<T: CFConvertible>(
    dict: &CFDictionary<CFString, CFType>,
    key: CFStringRef,
) -> DataResult<T> {
    match dict.find(key) {
        None => Err(DataError::key_not_found(key)),
        Some(item) => match item.downcast::<T::CF>() {
            None => Err(DataError::WrongType(T::expected_type())),
            Some(item) => T::convert(item),
        },
    }
}

/// Trait for types that can be converted from some `CFType`.
trait CFConvertible: Sized {
    /// Required source type for conversion.
    type CF: ConcreteCFType;

    /// Convert item to rust value.
    fn convert(cf: Self::CF) -> DataResult<Self>;

    /// Get expected type name for debugging/logging purposes.
    fn expected_type() -> String;
}

impl CFConvertible for String {
    type CF = CFString;

    fn convert(cf: Self::CF) -> DataResult<Self> {
        Ok(cf.to_string())
    }

    fn expected_type() -> String {
        "CFString".into()
    }
}

impl CFConvertible for i64 {
    type CF = CFNumber;

    fn convert(cf: Self::CF) -> DataResult<Self> {
        cf.to_i64()
            .ok_or_else(|| DataError::WrongType("SInt64".into()))
    }

    fn expected_type() -> String {
        "CFNumber".into()
    }
}

impl CFConvertible for bool {
    type CF = CFNumber;

    fn convert(cf: Self::CF) -> DataResult<Self> {
        i64::convert(cf).map(|i| i != 0)
    }

    fn expected_type() -> String {
        "CFNumber".into()
    }
}

impl<T: CFConvertible> CFConvertible for Vec<T> {
    type CF = CFArray;

    fn convert(cf: Self::CF) -> DataResult<Self> {
        let mut vec = Self::new();
        for item in cf.iter() {
            let item = unsafe { CFType::from_void(*item) };
            match item.downcast::<T::CF>() {
                None => return Err(DataError::WrongType(T::expected_type())),
                Some(item) => match T::convert(item) {
                    Err(err) => return Err(err),
                    Ok(item) => vec.push(item),
                },
            }
        }
        Ok(vec)
    }

    fn expected_type() -> String {
        "CFArray".into()
    }
}

/// Result alias for data conversion errors.
type DataResult<T> = std::result::Result<T, DataError>;

/// Error during data conversion.
#[derive(Debug)]
enum DataError {
    /// Given key wasn't found in a `CFDictionary`.
    KeyNotFound(String),
    /// Item has the wrong `CFType`, contains the expected type.
    WrongType(String),
}

impl Error for DataError {}

impl fmt::Display for DataError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            DataError::KeyNotFound(key) => write!(f, "key \"{}\" not found", key),
            DataError::WrongType(exp) => write!(f, "wrong type found, expected {}", exp),
        }
    }
}

impl DataError {
    /// Creates `DataError::KeyNotFound` variant from a `CFStringRef` key name.
    pub fn key_not_found(key: CFStringRef) -> Self {
        let key = unsafe { CFString::wrap_under_get_rule(key) };
        DataError::KeyNotFound(key.to_string())
    }
}

#[cfg(test)]
mod tests {
    use url::Url;

    use super::{MacOSXConfig, ProxyConfig};

    #[test]
    fn test_config() {
        let config = MacOSXConfig::new();
        let _proxies = config
            .get_config(&Url::parse("https://example.com").unwrap())
            .expect("failed to get config");
    }

    #[test]
    fn test_ignore() {
        let config = MacOSXConfig::new();
        let _ignore = config
            .get_ignore(&Url::parse("https://example.com").unwrap())
            .expect("failed to get config");
    }
}
