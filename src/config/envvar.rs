//! Extension for getting the proxy configuration from environment variables.

use std::env;

use url::Url;

use super::{ProxyConfig, ProxyConfigError, Result};

/// Log tag for this extension.
const LOG_TAG: &str = module_path!();

/// [`ProxyConfig`] implementation for getting the proxy configuration from environment variables.
///
/// [`ProxyConfig`]: ../trait.ProxyConfig.html
#[allow(missing_copy_implementations)]
pub struct EnvVarConfig;

impl EnvVarConfig {
    /// Create new instance.
    pub const fn new() -> Self {
        Self {}
    }
}

impl Default for EnvVarConfig {
    fn default() -> Self {
        Self::new()
    }
}

impl ProxyConfig for EnvVarConfig {
    fn get_config(&self, url: &Url) -> Result<Vec<Url>> {
        // read environment vars based on URL scheme
        let mut env_result = match url.scheme() {
            "ftp" => get_env_var("ftp_proxy", "FTP_PROXY"),
            "https" => get_env_var("https_proxy", "HTTPS_PROXY"),
            _ => get_env_var("http_proxy", "HTTP_PROXY"),
        };
        // if that didn't work, try the default http proxy
        if env_result.is_err() {
            env_result = get_env_var("http_proxy", "HTTP_PROXY");
        }
        match env_result {
            Ok(proxy) => Ok(vec![Url::parse(&proxy)?]),
            Err(env::VarError::NotPresent) => Err(ProxyConfigError::FailedToReadConfiguration),
            Err(err) => Err(ProxyConfigError::InternalError(Box::new(err))),
        }
    }

    fn get_ignore(&self, _url: &Url) -> Result<String> {
        get_env_var("no_proxy", "NO_PROXY").map_err(ProxyConfigError::from_error)
    }

    fn log_tag(&self) -> &'static str {
        LOG_TAG
    }
}

/// Get value of an environment variable or a fallback variable if the first doesn't exist.
fn get_env_var(var1: &str, var2: &str) -> std::result::Result<String, env::VarError> {
    match env::var(var1) {
        Ok(val) => Ok(val),
        Err(_) => env::var(var2),
    }
}
