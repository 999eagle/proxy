//! Extension for getting the proxy configuration from the Windows registry.

use std::collections::HashMap;

use url::Url;
use winreg::{RegKey, RegValue};

use crate::util;

use super::{ProxyConfig, ProxyConfigError, Result};

/// Log tag for this extension.
const LOG_TAG: &str = module_path!();
/// Registry key containing internet settings.
const REG_KEY_SETTINGS: &str = r"Software\Microsoft\Windows\CurrentVersion\Internet Settings";
/// Registry key containing connections.
const REG_KEY_CONNECTIONS: &str =
    r"Software\Microsoft\Windows\CurrentVersion\Internet Settings\Connections";
/// Offset in a bit field for using PAC.
const OFFSET_PAC: u8 = (1 << 2);
/// Offset in a bit field for using WPAD.
const OFFSET_WPAD: u8 = (1 << 3);

/// [`ProxyConfig`] implementation for getting the proxy configuration from the Windows registry.
///
/// [`ProxyConfig`]: ../trait.ProxyConfig.html
#[allow(missing_copy_implementations)]
pub struct W32RegConfig;

impl W32RegConfig {
    /// Create new instance.
    pub const fn new() -> Self {
        Self {}
    }
}

impl Default for W32RegConfig {
    fn default() -> Self {
        Self::new()
    }
}

impl ProxyConfig for W32RegConfig {
    fn get_config(&self, url: &Url) -> Result<Vec<Url>> {
        match proxy_autoconfig_type() {
            AutoconfigType::WPAD => Ok(vec![util::wpad_url()]),
            AutoconfigType::PAC => {
                let mut url = "pac+".to_owned();
                let proxy_url: String = RegKey::predef(winreg::enums::HKEY_CURRENT_USER)
                    .open_subkey(REG_KEY_SETTINGS)
                    .map_err(ProxyConfigError::from_error)?
                    .get_value("AutoConfigURL")
                    .map_err(ProxyConfigError::from_error)?;
                url.push_str(&proxy_url);
                Ok(vec![Url::parse(&url)?])
            }
            AutoconfigType::None => {
                let reg_key = RegKey::predef(winreg::enums::HKEY_CURRENT_USER)
                    .open_subkey(REG_KEY_SETTINGS)
                    .map_err(ProxyConfigError::from_error)?;
                let enabled: u32 = reg_key
                    .get_value("ProxyEnable")
                    .map_err(ProxyConfigError::from_error)?;
                if enabled == 0 {
                    Ok(vec![util::direct_url()])
                } else {
                    let proxies: String = reg_key
                        .get_value("ProxyServer")
                        .map_err(ProxyConfigError::from_error)?;
                    let proxies = parse_proxy_server(&proxies);

                    if let Some(proxy) = proxies.get(url.scheme()) {
                        Ok(vec![proxy.clone()])
                    } else if let Some(proxy) = proxies.get("http") {
                        Ok(vec![proxy.clone()])
                    } else if let Some(proxy) = proxies.get("socks") {
                        Ok(vec![proxy.clone()])
                    } else {
                        Err(ProxyConfigError::InvalidConfig)
                    }
                }
            }
        }
    }

    fn get_ignore(&self, _url: &Url) -> Result<String> {
        let whitelist: String = RegKey::predef(winreg::enums::HKEY_CURRENT_USER)
            .open_subkey(REG_KEY_SETTINGS)
            .map_err(ProxyConfigError::from_error)?
            .get_value("ProxyOverride")
            .map_err(ProxyConfigError::from_error)?;
        if &whitelist == "<local>" {
            Ok(whitelist)
        } else {
            Ok("".to_owned())
        }
    }

    fn log_tag(&self) -> &'static str {
        LOG_TAG
    }
}

/// Autoconfiguration type specified in registry.
enum AutoconfigType {
    /// Use PAC.
    PAC,
    /// Use WPAD.
    WPAD,
    /// Use static proxy.
    None,
}

/// Get autoconfiguration type specified in registry.
fn proxy_autoconfig_type() -> AutoconfigType {
    RegKey::predef(winreg::enums::HKEY_CURRENT_USER)
        .open_subkey(REG_KEY_CONNECTIONS)
        .map(|key| match key.get_raw_value("DefaultConnectionSettings") {
            Ok(RegValue { ref bytes, .. }) if bytes.len() > 8 => {
                // we've checked the length already
                #[allow(clippy::indexing_slicing)]
                {
                    if (bytes[8] & OFFSET_PAC) == OFFSET_PAC {
                        AutoconfigType::PAC
                    } else if (bytes[8] & OFFSET_WPAD) == OFFSET_WPAD {
                        AutoconfigType::WPAD
                    } else {
                        AutoconfigType::None
                    }
                }
            }
            _ => AutoconfigType::None,
        })
        .unwrap_or(AutoconfigType::None)
}

/// Parse proxy servers to use for each URL scheme from a single semicolon-separated string. Returns
/// a `HashMap` containing the proxy URL to use for each URL scheme.
fn parse_proxy_server(proxy: &str) -> HashMap<String, Url> {
    let mut map = HashMap::new();
    for part in proxy.split(';').map(str::trim) {
        let (scheme, proxy) = match part.find('=') {
            Some(index) => {
                let (scheme, proxy) = part.split_at(index);
                // this always works because the '=' is the first char of `proxy`
                #[allow(clippy::indexing_slicing)]
                (scheme, &proxy[1..])
            }
            None => ("http", part),
        };
        if let Ok(url) = util::parse_url_with_default_scheme(proxy, scheme) {
            #[allow(unused_results)]
            {
                map.insert(scheme.to_owned(), url);
            }
        }
    }
    map
}
