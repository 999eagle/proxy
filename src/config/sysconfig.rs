//! Extension for getting the proxy configuration from sysconfig.

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

use url::Url;

use super::{ProxyConfig, ProxyConfigError, Result};

use crate::util;

/// Log tag for this extension.
const LOG_TAG: &str = module_path!();
/// Default path for the sysconfig proxy file.
pub const DEFAULT_PATH: &str = "/etc/sysconfig/proxy";

/// [`ProxyConfig`] implementation for getting the proxy configuration from sysconfig.
///
/// [`ProxyConfig`]: ../trait.ProxyConfig.html
pub struct SysconfigConfig {
    /// Data read from the sysconfig proxy file.
    data: HashMap<String, String>,
}

impl SysconfigConfig {
    /// Create new instance with the default path.
    pub fn new() -> Result<Self> {
        Self::get_from_file(DEFAULT_PATH)
    }

    /// Create new instance with values read from the specified file.
    pub fn get_from_file<P: AsRef<Path>>(path: P) -> Result<Self> {
        let data = read_sysconfig(path)?;
        Ok(Self { data })
    }
}

impl ProxyConfig for SysconfigConfig {
    fn get_config(&self, url: &Url) -> Result<Vec<Url>> {
        // check whether proxy is enabled or not
        if let Some(enabled) = self.data.get("PROXY_ENABLED") {
            match enabled.as_str() {
                "no" | "NO" => return Ok(vec![util::direct_url()]),
                "yes" | "YES" => {} // continue running function
                _ => return Err(ProxyConfigError::InvalidConfig),
            }
        }

        let key = match url.scheme() {
            "ftp" => "FTP_PROXY",
            "https" => "HTTPS_PROXY",
            "http" | _ => "HTTP_PROXY",
        };
        match self.data.get(key) {
            Some(proxy) => Ok(vec![Url::parse(proxy)?]),
            None => Err(ProxyConfigError::InvalidConfig),
        }
    }

    fn get_ignore(&self, _url: &Url) -> Result<String> {
        match self.data.get("NO_PROXY") {
            Some(ignore) => Ok(ignore.clone()),
            None => Ok("".into()),
        }
    }

    fn log_tag(&self) -> &'static str {
        LOG_TAG
    }
}

/// Read sysconfig file and return the contained variables as `HashMap`.
fn read_sysconfig<P: AsRef<Path>>(path: P) -> Result<HashMap<String, String>> {
    let mut map = HashMap::new();
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    for line in reader.lines() {
        let line = line?;
        // ignore comments and empty lines
        if line.is_empty() || line.starts_with('#') {
            continue;
        }
        // split key and value at '=' sign
        match line.find('=') {
            None => continue,
            Some(index) => {
                let (key, value) = line.split_at(index);
                // value always contains at least the '=', so this works
                #[allow(clippy::indexing_slicing)]
                let (key, mut value) = (key.trim(), value[1..].trim());
                // trim enclosing `"` or `'` from value
                if value.len() >= 2 && (value.starts_with('"') && value.ends_with('"'))
                    || (value.starts_with('\'') && value.ends_with('\''))
                {
                    // we've checked the length
                    #[allow(clippy::indexing_slicing)]
                    {
                        value = &value[1..value.len() - 2];
                    }
                }
                // store key and value
                #[allow(unused_results)]
                {
                    map.insert(key.into(), value.into());
                }
            }
        }
    }
    Ok(map)
}
