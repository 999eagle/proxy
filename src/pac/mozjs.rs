//!  Extension for running PAC scripts using Mozilla's `mozjs` Javascript engine.
#![allow(unsafe_code)]

use std::ffi::{CStr, CString};
use std::io::Read;
use std::sync::{Arc, Mutex};

use log::{debug, info, trace};
use mozjs::conversions::{ConversionResult, FromJSValConvertible, ToJSValConvertible};
use mozjs::jsapi::{
    CallArgs, CompartmentOptions, HandleValueArray, Heap, JSAutoCompartment, JSClass, JSClassOps,
    JSContext, JSFunctionSpec, JSNativeWrapper, JSObject, JS_ClearPendingException,
    JS_NewGlobalObject, JS_ReportErrorUTF8, OnNewGlobalHookOption, Value, JSPROP_ENUMERATE,
};
use mozjs::jsval::UndefinedValue;
use mozjs::rooted;
use mozjs::rust::{
    jsapi_wrapped::{
        JS_CallFunctionName, JS_ErrorFromException, JS_GetPendingException, JS_InitStandardClasses,
    },
    Handle, JSEngine, MutableHandle, MutableHandleValue, Runtime, SIMPLE_GLOBAL_CLASS,
};
use url::Url;

use lazy_static::lazy_static;

use crate::error::{ProxyConfigError, Result};

use super::{PacRunner, PacRunnerBuilder};

lazy_static! {
    static ref ENGINE: Mutex<Option<Arc<JSEngine>>> = Mutex::new(None);
}

/// Log tag for this extension.
const LOG_TAG: &str = module_path!();

/// [`PacRunnerBuilder`] implementation for running PAC scripts using Mozilla's `mozjs` Javascript engine.
///
/// [`PacRunnerBuilder`]: ../trait.PacRunnerBuilder.html
#[allow(missing_copy_implementations)]
pub struct MozJSBuilder;

impl MozJSBuilder {
    /// Create new instance.
    pub const fn new() -> Self {
        Self
    }
}

impl Default for MozJSBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl PacRunnerBuilder for MozJSBuilder {
    fn build_pac_runner(&self, pac_script: &mut dyn Read) -> Result<Box<dyn PacRunner>> {
        let runner = MozJS::new(pac_script)?;
        Ok(Box::new(runner))
    }

    fn log_tag(&self) -> &'static str {
        LOG_TAG
    }
}

/// [`PacRunner`] implementation for running PAC scripts using Mozilla's `mozjs` Javascript engine.
///
/// The order of fields is important as rust drops its fields in the order they are declared
/// when dropping the `MozJS`, `ac` has to be dropped before `global` has to be dropped before `runtime`!
///
/// [`PacRunner`]: ../trait.PacRunner.html
pub struct MozJS {
    /// Keeps the compartment containing the global object active until dropped.
    _ac: JSAutoCompartment,
    /// Contains the global object in the JS context.
    global: Box<Heap<*mut JSObject>>,
    /// JS Runtime to use for PAC execution.
    runtime: Runtime,
}

impl MozJS {
    /// Create new instance.
    pub fn new(script: &mut dyn Read) -> Result<Self> {
        let mut engine_guard = ENGINE
            .lock()
            .expect("mozjs engine has been poisoned, can't continue");
        if engine_guard.is_none() {
            // TODO: remove this when this lint works again
            #[allow(unused_results)]
            {
                engine_guard.replace(JSEngine::init()?);
            }
            info!("initialized mozjs engine");
        }
        #[allow(clippy::option_unwrap_used)] // we've handled the None case above
        let engine = Arc::clone(engine_guard.as_ref().unwrap());
        let runtime = Runtime::new(engine);
        let cx = runtime.cx();

        let (global, ac) = unsafe {
            // create the global object that contains the predefined functions for PAC scripts
            let compartment_options = CompartmentOptions::default();
            let global = Heap::boxed(JS_NewGlobalObject(
                cx,
                &GLOBAL_CLASS,
                std::ptr::null_mut(),
                OnNewGlobalHookOption::FireOnNewGlobalHook,
                &compartment_options,
            ));
            rooted!(in(cx) let global_rooted = global.get());
            // enter the global object's compartment to be able to modify it
            let ac = JSAutoCompartment::new(cx, global_rooted.get());
            (global, ac)
        };
        trace!("created js global object");

        unsafe {
            rooted!(in(cx) let global = global.get());
            // define standard stuff on global objects
            if !JS_InitStandardClasses(cx, global.handle()) {
                return Err(ProxyConfigError::JSInternalError(
                    "failed to init standard classes".into(),
                ));
            }
            // define the necessary functions on the global object
            mozjs::rust::define_methods(cx, global.handle(), GLOBAL_METHODS)
                .err_msg("failed to define global methods")?;
            // evaluate javascript functions
            rooted!(in(cx) let mut rval = UndefinedValue());
            runtime
                .evaluate_script(
                    global.handle(),
                    super::util::JAVASCRIPT_FUNCTIONS,
                    "globals",
                    1,
                    rval.handle_mut(),
                )
                .err_msg("failed to set up javascript functions")?;
            #[cfg(test)]
            {
                runtime
                    .evaluate_script(
                        global.handle(),
                        super::util::JAVASCRIPT_FUNCTIONS_TEST,
                        "globals",
                        1,
                        rval.handle_mut(),
                    )
                    .err_msg("failed to set up javascript functions for tests")?;
            }
            trace!("initialized global object");
            // evaluate pac script
            let mut script_str = String::new();
            // TODO: remove this when this lint works again
            #[allow(unused_results)]
            {
                script.read_to_string(&mut script_str)?;
            }
            runtime
                .evaluate_script(global.handle(), &script_str, "pac", 1, rval.handle_mut())
                .err_msg("failed to evaluate pac script")?;
            debug!("evaluated pac script");
        }

        Ok(Self {
            runtime,
            global,
            _ac: ac,
        })
    }
}

impl PacRunner for MozJS {
    fn get_proxy_for_url(&mut self, url: &Url) -> Result<String> {
        let cx = self.runtime.cx();
        rooted!(in(cx) let global = self.global.get());
        // prepare the function arguments
        let host = url
            .host_str()
            .ok_or_else(|| ProxyConfigError::Message("url doesn't have a host".into()))?
            .to_string();
        let url = url.as_str().to_string();
        trace!("prepared pac function arguments");
        let result = unsafe {
            // convert arguments to JSValue and to array
            rooted!(in(cx) let mut arg_host = UndefinedValue());
            rooted!(in(cx) let mut arg_url = UndefinedValue());
            host.to_jsval(cx, arg_host.handle_mut());
            url.as_str().to_string().to_jsval(cx, arg_url.handle_mut());
            let args = HandleValueArray::from_rooted_slice(&[arg_url.get(), arg_host.get()]);
            trace!("created args array");
            // return value
            rooted!(in(cx) let mut rval = UndefinedValue());
            // call PAC function
            let result = JS_CallFunctionName(
                cx,
                global.handle(),
                PROXY_FUNCTION_NAME,
                &args,
                &mut rval.handle_mut(),
            );
            trace!("called pac function with result: {}", result);
            if !result {
                return Err(get_exception(cx, "exception in pac script"));
            }
            String::from_jsval(cx, rval.handle(), ())
        };
        match result {
            Ok(ConversionResult::Success(proxy)) => Ok(proxy),
            Ok(ConversionResult::Failure(err)) => {
                Err(ProxyConfigError::JSInternalError(err.to_string()))
            }
            Err(()) => Err(ProxyConfigError::JSInternalError(
                "pac script didn't return a string".into(),
            )),
        }
    }
}

/// Get last exception from the `JSContext` and returns it with an added custom error message as
/// [`ProxyConfigError`].
///
/// [`ProxyConfigError`]: ../../error/enum.ProxyConfigError.html
unsafe fn get_exception(cx: *mut JSContext, msg: &str) -> ProxyConfigError {
    rooted!(in(cx) let mut exception = UndefinedValue());
    if !JS_GetPendingException(cx, &mut exception.handle_mut()) {
        return ProxyConfigError::JSInternalError(msg.into());
    }
    JS_ClearPendingException(cx);
    debug!("reporting error");
    if !exception.handle().is_object() {
        return ProxyConfigError::JSInternalError(format!("{}: unkown error", msg));
    }

    rooted!(in(cx) let object = exception.handle().to_object());
    let error_report = JS_ErrorFromException(cx, object.handle());
    #[allow(clippy::used_underscore_binding)]
    let base = &(*error_report)._base;
    let filename = CStr::from_ptr(base.filename).to_str().unwrap_or("");
    let message = CStr::from_ptr(base.message_.data_).to_str().unwrap_or("");
    ProxyConfigError::JSInternalError(format!(
        "{}: js error in {} at {}:{}: \"{}\"",
        msg, filename, base.lineno, base.column, message
    ))
}

/// Helper trait to convert the JS engine's results with `()` as `Err` type to an error message.
trait JSResult<T> {
    /// Return result with an error message on `Err`.
    fn err_msg(self, msg: &'static str) -> Result<T>;
}

impl<T> JSResult<T> for std::result::Result<T, ()> {
    fn err_msg(self, msg: &'static str) -> Result<T> {
        self.map_err(|_| ProxyConfigError::JSInternalError(msg.into()))
    }
}

/// Operations for the global object's class.
static GLOBAL_CLASS_OPS: JSClassOps = JSClassOps {
    addProperty: None,
    delProperty: None,
    enumerate: None,
    newEnumerate: None,
    resolve: None,
    mayResolve: None,
    finalize: None,
    call: None,
    hasInstance: None,
    construct: None,
    trace: None,
};

/// Definition of the global object's class.
static GLOBAL_CLASS: JSClass = JSClass {
    name: b"global\0" as *const u8 as *const libc::c_char,
    flags: SIMPLE_GLOBAL_CLASS.flags,
    cOps: &GLOBAL_CLASS_OPS,
    reserved: [0 as *mut _; 3],
};

/// Methods to add to the global object.
const GLOBAL_METHODS: &[JSFunctionSpec] = &[
    JSFunctionSpec {
        name: b"dnsResolve\0" as *const u8 as *const libc::c_char,
        call: JSNativeWrapper {
            op: Some(dns_resolve),
            info: std::ptr::null(),
        },
        nargs: 1,
        flags: JSPROP_ENUMERATE as u16,
        selfHostedName: std::ptr::null(),
    },
    JSFunctionSpec {
        name: b"myIpAddress\0" as *const u8 as *const libc::c_char,
        call: JSNativeWrapper {
            op: Some(my_ip_address),
            info: std::ptr::null(),
        },
        nargs: 0,
        flags: JSPROP_ENUMERATE as u16,
        selfHostedName: std::ptr::null(),
    },
    JSFunctionSpec::ZERO,
];

/// Name of the entry point function in PAC scripts.
const PROXY_FUNCTION_NAME: *const libc::c_char =
    b"FindProxyForURL\0" as *const u8 as *const libc::c_char;

/// Report an error message to the `JSContext`.
fn report_error(cx: *mut JSContext, error: &str) -> bool {
    let error = CString::new(error).unwrap_or_else(|err| {
        CString::new(format!("error when reporting error: {}", err)).expect("this can't happen")
    });
    unsafe {
        JS_ReportErrorUTF8(cx, error.as_ptr());
    }
    false
}

/// Implementation for resolving a DNS name.
fn dns_resolve_internal(
    cx: *mut JSContext,
    rval: MutableHandleValue<'_>,
    host: &str,
) -> Result<()> {
    let ip = dns_lookup::lookup_host(host)?
        .first()
        .ok_or_else(|| ProxyConfigError::Message("dns resolve failed".into()))?
        .to_string();
    unsafe {
        ip.to_jsval(cx, rval);
    }
    Ok(())
}

/// Implementation for getting the current IP address.
fn my_ip_address_internal(cx: *mut JSContext, rval: MutableHandleValue<'_>) -> Result<()> {
    let hostname = dns_lookup::get_hostname()?;
    dns_resolve_internal(cx, rval, hostname.as_str())
}

/// Extern function to be called from JS for resolving a DNS name.
unsafe extern "C" fn dns_resolve(cx: *mut JSContext, argc: u32, vp: *mut Value) -> bool {
    // https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey/JSAPI_reference/JSNative
    let args = CallArgs::from_vp(vp, argc);
    trace!("calling dns_resolve");

    if args.argc_ != 1 {
        return report_error(cx, "dns_resolve() requires exactly 1 argument");
    }

    let arg = Handle::from_raw(args.get(0));
    let host = match String::from_jsval(cx, arg, ()) {
        Ok(ConversionResult::Success(val)) => val,
        Ok(ConversionResult::Failure(err)) => {
            return report_error(cx, &format!("failed to get argument: {}", err));
        }
        Err(()) => {
            return report_error(cx, "failed to get argument");
        }
    };
    debug!("dns_resolve called with argument {}", &host);
    let rval = MutableHandle::from_raw(args.rval());
    match dns_resolve_internal(cx, rval, host.as_str()) {
        Ok(()) => true,
        Err(err) => report_error(cx, &format!("error: {}", err)),
    }
}

/// Extern function to be called from JS for getting the current IP address.
unsafe extern "C" fn my_ip_address(cx: *mut JSContext, argc: u32, vp: *mut Value) -> bool {
    // https://developer.mozilla.org/en-US/docs/Mozilla/Projects/SpiderMonkey/JSAPI_reference/JSNative
    let args = CallArgs::from_vp(vp, argc);
    trace!("calling my_ip_address");

    if args.argc_ != 0 {
        return report_error(cx, "my_ip_address() requires exactly 0 arguments");
    }
    debug!("my_ip_address called");
    let rval = MutableHandle::from_raw(args.rval());
    match my_ip_address_internal(cx, rval) {
        Ok(()) => true,
        Err(err) => report_error(cx, &format!("error: {}", err)),
    }
}

#[cfg(test)]
mod tests {
    use std::net::IpAddr;
    use std::str::FromStr;

    use url::Url;

    use super::{MozJS, PacRunner};

    #[test]
    fn it_works() {
        let script = b"function FindProxyForURL(url, host){ return getRandomNumber(); }".to_vec();
        let mut runner = MozJS::new(&mut &script[..]).expect("failed to create runner");
        let url = Url::parse("https://www.xkcd.com/221").expect("failed to parse URL");
        let value = runner
            .get_proxy_for_url(&url)
            .expect("failed to run script");
        assert_eq!(value, "4");
    }

    #[test]
    fn test_dns_resolve() {
        let script = b"function FindProxyForURL(url, host){ return dnsResolve(host); }".to_vec();
        let mut runner = MozJS::new(&mut &script[..]).expect("failed to create runner");
        let url = Url::parse("http://localhost").expect("failed to parse URL");
        let value = runner
            .get_proxy_for_url(&url)
            .expect("failed to run script");
        match value.as_str() {
            "127.0.0.1" | "::1" => {}
            act => panic!("expected 127.0.0.1 or ::1, but got: {}", act),
        }
    }

    #[test]
    fn test_my_ip_address() {
        let script = b"function FindProxyForURL(url, host){ return myIpAddress(); }".to_vec();
        let mut runner = MozJS::new(&mut &script[..]).expect("failed to create runner");
        let url = Url::parse("http://example.com").expect("failed to parse URL");
        let value = runner
            .get_proxy_for_url(&url)
            .expect("failed to run script");
        IpAddr::from_str(value.as_str()).expect("failed to parse returned ip address");
    }
}
