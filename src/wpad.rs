//! Extensions for handling WPAD.
//!
//! All extensions must implement the [`WPAD`] trait defined here.
//!
//! [`WPAD`]: trait.WPAD.html

use std::io::Read;

use crate::error::{ProxyConfigError, Result};

pub mod dns_alias;

/// Trait for WPAD extensions. Extensions are responsible for getting a PAC script using WPAD.
pub trait WPAD {
    /// Get script from WPAD.
    fn get_wpad(&self) -> Result<Box<dyn Read>>;
    /// Get log tag for this extension.
    fn log_tag(&self) -> &'static str;
}
