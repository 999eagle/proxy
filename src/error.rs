//! Error implementations for this crate.

use std::{error::Error, fmt};

/// Result type alias for use with [`ProxyConfigError`].
///
/// ['ProxyConfigError']: enum.ProxyConfigError.html
pub type Result<T> = std::result::Result<T, ProxyConfigError>;

/// Main error type.
#[derive(Debug)]
pub enum ProxyConfigError {
    /// Failed to read some configuration source.
    FailedToReadConfiguration,
    /// Wrapping another unspecified Rust error.
    InternalError(Box<dyn Error>),
    /// Invalid configuration encountered.
    InvalidConfig,
    /// Failed to parse an URL.
    UrlParseError(url::ParseError),
    /// No configuration available.
    NoConfiguration,
    /// IO Error.
    IoError(std::io::Error),
    /// Unspecified error with an error message.
    Message(String),
    /// WPAD is not enabled.
    WpadNotEnabled,
    /// PAC is not enabled.
    PacNotEnabled,
    /// Error from a Javascript engine.
    JSEngineError(JSEngineError),
    /// Error during Javascript execution.
    JSInternalError(String),
    /// Invalid proxy type returned by PAC script.
    InvalidPacProxyType(String),
}

#[allow(clippy::empty_enum, missing_copy_implementations)]
/// Error from a Javascript engine.
#[derive(Debug)]
pub enum JSEngineError {
    #[cfg(feature = "spidermonkey")]
    /// Error in mozjs.
    MozJS(mozjs::rust::JSEngineError),
}

impl ProxyConfigError {
    /// Wraps any error implementing the [`std::error::Error`] Trait.
    pub fn from_error<E>(error: E) -> Self
    where
        E: Error + 'static,
    {
        ProxyConfigError::InternalError(Box::new(error))
    }
}

impl Error for ProxyConfigError {}

impl fmt::Display for ProxyConfigError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ProxyConfigError::FailedToReadConfiguration => {
                write!(f, "failed to read configuration")
            }
            ProxyConfigError::InternalError(err) => write!(f, "proxy configuration error: {}", err),
            ProxyConfigError::InvalidConfig => write!(f, "invalid configuration"),
            ProxyConfigError::UrlParseError(err) => write!(f, "failed to parse url: {}", err),
            ProxyConfigError::NoConfiguration => write!(f, "no configuration available"),
            ProxyConfigError::IoError(err) => write!(f, "io error: {}", err),
            ProxyConfigError::WpadNotEnabled => write!(f, "WPAD support is not enabled"),
            ProxyConfigError::PacNotEnabled => write!(f, "PAC support is not enabled"),
            ProxyConfigError::JSEngineError(err) => write!(f, "js engine error: {}", err),
            ProxyConfigError::JSInternalError(msg) => write!(f, "js internal error: {}", msg),
            ProxyConfigError::InvalidPacProxyType(t) => write!(f, "invalid pac proxy type {}", t),
            ProxyConfigError::Message(msg) => write!(f, "{}", msg),
        }
    }
}

impl fmt::Display for JSEngineError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        #[allow(clippy::use_debug, unreachable_patterns)]
        match self {
            #[cfg(feature = "spidermonkey")]
            JSEngineError::MozJS(e) => write!(f, "{:?}", e),
            _ => write!(f, "unknown engine error"),
        }
    }
}

impl From<url::ParseError> for ProxyConfigError {
    fn from(e: url::ParseError) -> Self {
        ProxyConfigError::UrlParseError(e)
    }
}

impl From<std::io::Error> for ProxyConfigError {
    fn from(e: std::io::Error) -> Self {
        ProxyConfigError::IoError(e)
    }
}

#[cfg(feature = "spidermonkey")]
impl From<mozjs::rust::JSEngineError> for ProxyConfigError {
    fn from(e: mozjs::rust::JSEngineError) -> Self {
        ProxyConfigError::JSEngineError(JSEngineError::MozJS(e))
    }
}
