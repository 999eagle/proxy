//! Crate for getting the current system proxy configuration.

// These are outright wrong and are forbidden to be allowed. This forces correct code.
// If there is indeed a false positive in one of these, they can still be moved to the deny section.
#![forbid(
    const_err,
    exceeding_bitshifts,
    future_incompatible,
    irrefutable_let_patterns,
    macro_use_extern_crate,
    mutable_transmutes,
    no_mangle_const_items,
    nonstandard_style,
    rust_2018_compatibility,
    rust_2018_idioms,
    trivial_casts,
    trivial_numeric_casts,
    unknown_crate_types,
    unused,
    unused_import_braces,
    unused_lifetimes,
    deprecated,
    illegal_floating_point_literal_pattern,
    improper_ctypes,
    intra_doc_link_resolution_failure,
    irrefutable_let_patterns,
    late_bound_lifetime_arguments,
    non_shorthand_field_patterns,
    non_snake_case,
    non_upper_case_globals,
    no_mangle_generic_items,
    overflowing_literals,
    path_statements,
    patterns_in_fns_without_body,
    plugin_as_library,
    private_in_public,
    proc_macro_derive_resolution_fallback,
    renamed_and_removed_lints,
    safe_packed_borrows,
    stable_features,
    trivial_bounds,
    type_alias_bounds,
    tyvar_behind_raw_pointer,
    unconditional_recursion,
    unions_with_drop_fields,
    unknown_lints,
    unnameable_test_items,
    unstable_name_collisions,
    where_clauses_object_safety,
    while_true,
    clippy::complexity,
    clippy::correctness,
    clippy::perf,
    clippy::style
)]
// These are denied so one is forced to annotate expected behaviour by allowing it.
#![deny(
    dead_code,
    missing_copy_implementations,
    non_camel_case_types,
    unconditional_recursion,
    unreachable_code,
    unreachable_patterns,
    unused_qualifications,
    unused_results,
    unsafe_code,
    variant_size_differences,
    clippy::cast_possible_truncation,
    clippy::cast_possible_wrap,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss,
    clippy::decimal_literal_representation,
    clippy::empty_line_after_outer_attr,
    clippy::eval_order_dependence,
    clippy::indexing_slicing,
    clippy::mutex_integer,
    clippy::needless_pass_by_value,
    clippy::non_ascii_literal,
    clippy::nursery,
    clippy::option_unwrap_used,
    clippy::pedantic,
    clippy::restriction,
    clippy::result_unwrap_used
)]
// These can either not be #[allow()]'d, have false-positives or are development-tools
// of which we only care about in the CI.
#![warn(
    missing_docs,
    clippy::float_arithmetic,
    clippy::inline_always,
    clippy::missing_docs_in_private_items,
    clippy::multiple_crate_versions,
    clippy::shadow_reuse,
    clippy::shadow_same,
    clippy::unimplemented,
    clippy::use_self
)]
#![allow(
    box_pointers,
    missing_debug_implementations,
    missing_doc_code_examples,
    unreachable_pub,
    clippy::cargo,
    clippy::implicit_return,
    clippy::integer_arithmetic,
    clippy::missing_inline_in_public_items,
    clippy::module_name_repetitions,
    clippy::similar_names,
    clippy::wildcard_enum_match_arm
)]
// Set this to `warn` again if https://github.com/rust-lang/rust/issues/54079 is resolved.
#![allow(single_use_lifetimes)]
// allow unwraps in tests
#![cfg_attr(
    test,
    allow(clippy::option_unwrap_used, clippy::result_unwrap_used, unused_results)
)]

use std::io::Read;

use log::{debug, error, warn};
use url::Url;

use self::error::{ProxyConfigError, Result};
use self::{
    config::ProxyConfig,
    pac::{PacRunner, PacRunnerBuilder},
    wpad::WPAD,
};

pub mod config;
pub mod error;
pub mod pac;
pub mod util;
pub mod wpad;

/// Builder for a [`ProxyFactory`].
///
/// [`ProxyFactory`]: struct.ProxyFactory.html
#[derive(Default)]
pub struct ProxyFactoryBuilder {
    /// Extensions for getting the proxy configuration.
    proxy_config: Vec<Box<dyn ProxyConfig>>,
    /// Extensions for handling WPAD.
    wpad: Vec<Box<dyn WPAD>>,
    /// Extensions for running PAC scripts.
    pac: Vec<Box<dyn PacRunnerBuilder>>,
    /// Whether to enable WPAD or not.
    enable_wpad: bool,
    /// Whether to enable PAC or not.
    enable_pac: bool,
}

impl ProxyFactoryBuilder {
    /// Creates a new instance.
    pub fn new() -> Self {
        Self {
            proxy_config: Vec::new(),
            wpad: Vec::new(),
            pac: Vec::new(),
            enable_wpad: false,
            enable_pac: false,
        }
    }

    /// Set whether to enable WPAD or not.
    pub const fn enable_wpad(mut self, enable_wpad: bool) -> Self {
        self.enable_wpad = enable_wpad;
        self
    }

    /// Set whether to enable PAC or not.
    pub const fn enable_pac(mut self, enable_pac: bool) -> Self {
        self.enable_pac = enable_pac;
        self
    }

    /// Add all included extensions if enabled (depends on OS and feature flags).
    pub fn add_default_extensions(mut self) -> Self {
        self.proxy_config
            .push(Box::new(self::config::envvar::EnvVarConfig::new()));
        if let Ok(config) = config::sysconfig::SysconfigConfig::new() {
            self.proxy_config.push(Box::new(config));
        }
        #[cfg(target_os = "windows")]
        self.proxy_config
            .push(Box::new(self::config::w32reg::W32RegConfig::new()));
        #[cfg(target_os = "macos")]
        self.proxy_config
            .push(Box::new(self::config::macosx::MacOSXConfig::new()));

        self.wpad
            .push(Box::new(self::wpad::dns_alias::DNSAlias::new()));

        #[cfg(feature = "spidermonkey")]
        self.pac
            .push(Box::new(self::pac::mozjs::MozJSBuilder::new()));

        self
    }

    /// Build a new [`ProxyFactory`] from this builder.
    ///
    /// [`ProxyFactory`]: struct.ProxyFactory.html
    pub fn build(self) -> ProxyFactory {
        ProxyFactory {
            proxy_config: self.proxy_config,
            wpad: self.wpad,
            pac: self.pac,
            pac_runner: None,
            enable_wpad: self.enable_wpad,
            enable_pac: self.enable_pac,
        }
    }
}

/// Gets proxies based on the configuration given in the [`ProxyFactoryBuilder`].
///
/// [`ProxyFactoryBuilder`]: struct.ProxyFactoryBuilder.html
pub struct ProxyFactory {
    /// Extensions for getting the proxy configuration.
    proxy_config: Vec<Box<dyn ProxyConfig>>,
    /// Extensions for handling WPAD.
    wpad: Vec<Box<dyn WPAD>>,
    /// Extensions for running PAC scripts.
    pac: Vec<Box<dyn PacRunnerBuilder>>,
    /// Current PAC runner.
    pac_runner: Option<Box<dyn PacRunner>>,
    /// Whether to enable WPAD or not.
    enable_wpad: bool,
    /// Whether to enable PAC or not.
    enable_pac: bool,
}

impl ProxyFactory {
    /// Get proxies for a given URL.
    pub fn get_proxies_for_url(&mut self, target_url: &Url) -> Result<Vec<Url>> {
        let mut proxies = Vec::new();
        let config = self.get_config(target_url);
        for config in config {
            let (scheme, proxy_url) = match config.scheme().find('+') {
                None => (config.scheme(), config.as_str()),
                Some(index) => {
                    let (scheme, proxy_url) = config.as_str().split_at(index);
                    // this always works because the '+' is the first char of `url`
                    #[allow(clippy::indexing_slicing)]
                    (scheme, &proxy_url[1..])
                }
            };
            let proxy_url = Url::parse(proxy_url)?;
            match scheme {
                "wpad" => {
                    if self.enable_wpad {
                        if let Some(mut pac_script) = self.get_wpad() {
                            self.build_pac_runner(&mut pac_script);
                            proxies.append(&mut self.run_pac_script(target_url)?);
                        }
                    } else {
                        return Err(ProxyConfigError::WpadNotEnabled);
                    }
                }
                "pac" => {
                    if self.enable_pac {
                        let result = util::get_pac_script_from_uri(proxy_url.as_str());
                        let mut pac_script = match result {
                            Ok(response) => match util::body_into_read(response.into_body()) {
                                Ok(reader) => reader,
                                Err(err) => {
                                    debug!("failed to get pac script: {}", err);
                                    return Err(ProxyConfigError::FailedToReadConfiguration);
                                }
                            },
                            Err(err) => {
                                debug!("failed to get pac script: {}", err);
                                return Err(ProxyConfigError::FailedToReadConfiguration);
                            }
                        };
                        self.build_pac_runner(&mut pac_script);
                        proxies.append(&mut self.run_pac_script(target_url)?);
                    } else {
                        return Err(ProxyConfigError::PacNotEnabled);
                    }
                }
                _ => proxies.push(proxy_url),
            }
        }
        if proxies.is_empty() {
            proxies.push(Url::parse("direct://")?)
        }
        Ok(proxies)
    }

    /// Query configuration extensions to get a list of proxies to use.
    fn get_config(&self, url: &Url) -> Vec<Url> {
        for configurator in &self.proxy_config {
            let ignore = match configurator.get_ignore(url) {
                Ok(ignore) => ignore,
                Err(err) => {
                    debug!(
                        "Configurator {} failed in get_ignore: {}",
                        configurator.log_tag(),
                        err
                    );
                    continue;
                }
            };
            let config = if self.is_ignored(url, &ignore) {
                vec![]
            } else {
                match configurator.get_config(url) {
                    Ok(config) => config,
                    Err(err) => {
                        debug!(
                            "Configurator {} failed in get_config: {}",
                            configurator.log_tag(),
                            err
                        );
                        continue;
                    }
                }
            };
            return config;
        }
        vec![]
    }

    /// Check whether the an URL matches an ignore string.
    #[allow(clippy::missing_const_for_fn)] // until this is implemented
    fn is_ignored(&self, _url: &Url, _ignore: &str) -> bool {
        // TODO
        false
    }

    /// Query WPAD extensions to find a PAC script.
    fn get_wpad(&self) -> Option<Box<dyn Read>> {
        debug!("trying to find PAC script using WPAD");
        for wpad in &self.wpad {
            match wpad.get_wpad() {
                Ok(pac_script) => return Some(pac_script),
                Err(err) => {
                    debug!("WPAD method {} failed: {}", wpad.log_tag(), err);
                    continue;
                }
            }
        }

        None
    }

    /// Use PAC runner extensions to build a PAC runner for the given script.
    fn build_pac_runner(&mut self, pac_script: &mut dyn Read) {
        debug_assert!(self.enable_pac, "pac not enabled");
        let mut pac_runner = None;
        for builder in &self.pac {
            match builder.build_pac_runner(pac_script) {
                Ok(runner) => {
                    pac_runner = Some(runner);
                    break;
                }
                Err(err) => {
                    debug!("PAC builder {} failed: {}", builder.log_tag(), err);
                    continue;
                }
            }
        }

        if pac_runner.is_none() {
            error!("no PAC runner available");
        }

        self.pac_runner = pac_runner;
    }

    /// Run a PAC script using a previously built PAC runner (see [`build_pac_runner`]) to get proxies for an URL.
    fn run_pac_script(&mut self, url: &Url) -> Result<Vec<Url>> {
        debug_assert!(self.enable_pac, "pac not enabled");
        let pac_runner = match self.pac_runner.as_mut() {
            Some(runner) => runner,
            None => return Err(ProxyConfigError::Message("no PAC runner available".into())),
        };
        let pac_result = pac_runner.get_proxy_for_url(url)?;
        let mut proxies = Vec::new();
        for proxy in pac_result.split(';').filter(|s| !s.is_empty()) {
            let split = proxy
                .split(|c: char| c == ' ' || c == '\t')
                .collect::<Vec<_>>();
            #[allow(clippy::indexing_slicing)]
            let proxy_type = split[0];
            let proxy_url = match proxy_type {
                "DIRECT" => Ok(util::direct_url()),
                "HTTP" | "PROXY" | "HTTPS" | "SOCKS" | "SOCKS4" | "SOCKS5" => {
                    let scheme = match proxy_type {
                        "PROXY" => "http".into(),
                        t => t.to_lowercase(),
                    };
                    match split.get(1) {
                        None => Err(ProxyConfigError::InvalidConfig),
                        Some(url) => util::parse_url_with_default_scheme(url, &scheme),
                    }
                }
                _ => Err(ProxyConfigError::InvalidPacProxyType(proxy_type.into())),
            };

            match proxy_url {
                Ok(proxy) => proxies.push(proxy),
                Err(err) => warn!("failed to parse proxy {}: {}", proxy, err),
            }
        }

        Ok(proxies)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        url::Url::parse("direct://").unwrap();
    }
}
