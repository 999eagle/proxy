//! Extensions for getting the proxy configuration.
//!
//! All extensions must implement the [`ProxyConfig`] trait defined here.
//!
//! [`ProxyConfig`]: trait.ProxyConfig.html

use crate::error::{ProxyConfigError, Result};

use url::Url;

pub mod envvar;
#[cfg(target_os = "macos")]
pub mod macosx;
pub mod sysconfig;
#[cfg(target_os = "windows")]
pub mod w32reg;

/// Trait for proxy configuration extensions. Extensions are responsible for reading the configuration from somewhere.
pub trait ProxyConfig {
    /// Get configuration for a given URL. Returns a `Vec` of proxy URLs to be used.
    fn get_config(&self, url: &Url) -> Result<Vec<Url>>;
    /// Get ignore string for a given URL. Returns a comma separated list of ignore strings.
    fn get_ignore(&self, url: &Url) -> Result<String>;
    /// Get log tag for this extension.
    fn log_tag(&self) -> &'static str;
}
