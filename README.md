# proxy

This crate is based on the libproxy project, but written in Rust. It supports reading the system default proxy settings on Linux, macOS and Windows including support for WPAD (Web Proxy Auto Discovery) and PAC (Proxy Auto Configuration). To be able to run PAC scripts, you must enable the feature `spidermonkey`.

## Examples

```rust
use proxy::ProxyFactoryBuilder;
use url::Url;

fn get_proxies(url: &str) -> Result<Vec<Url>> {
    let mut factory = ProxyFactoryBuilder::new()
        .enable_pac(true)
        .enable_wpad(true)
        .add_default_extensions()
        .build();
    factory.get_proxies_for_url(&Url::parse(&url)?)
}
```

## Building

The default feature set has no special requirements for compiling.

### Feature `spidermonkey`

If `spidermonkey` is enabled you need these requirements for compiling this crate depending on your platform:

- Linux
    - `autoconf 2.13`
    - `python 2`
    - `clang`
- macOS
    - `autoconf 2.13`
    - `python 2`
    - `clang`
    - `yasm`
- Windows
    - Install [Mozilla Build Tools](https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Build_Instructions/Windows_Prerequisites#Required_tools)
    - Install Build Tools for Visual Studio 2019 or Visual Studio 2019 from https://visualstudio.microsoft.com/downloads/ with these workloads:
        - Windows 10 SDK at least 10.0.17134.0
        - C++ ATL for v142 build tools (x86 and x64)
        - C++ development
    - Install [Clang for Windows (64 bit)](https://releases.llvm.org/download.html)
    - Build in a Visual Studio developer shell
        - Either use the one included with Visual Studio
        - or run `"C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat"` in a shell.
    - Set these environment variables:
        - `MOZTOOLS_PATH=C:\mozilla-build\msys\bin;C:\mozilla-build\bin`
        - `AUTOCONF=C:\mozilla-build\msys\local\bin\autoconf-2.13`
        - `NATIVE_WIN32_PYTHON=C:\mozilla-build\python\python2.7.exe`
        - `LIBCLANG_PATH=C:\Program Files\LLVM\lib`
    
